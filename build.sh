#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2019 Major Hayden
# SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: Apache-2.0

set -euxo pipefail

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# cf. https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container#setup
# tl~dr: make buildah aware it's already isolated in a container
export _BUILDAH_STARTED_IN_USERNS=""
export BUILDAH_ISOLATION=chroot

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${IMAGE_NAME}"
COMMIT_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}-${IMAGE_NAME}"

# start from the rust image
buildah pull docker.io/rust:${IMAGE_NAME}
CONTAINER_ID=$(buildah from docker.io/rust:${IMAGE_NAME})

# install cargo packages
buildah run $CONTAINER_ID -- cargo install --force --all-features cargo-make
buildah run $CONTAINER_ID -- cargo install --force --all-features cargo-tarpaulin
buildah run $CONTAINER_ID -- cargo install --force --all-features cargo-chef
buildah run $CONTAINER_ID -- cargo install --force --all-features cargo-audit
buildah run $CONTAINER_ID -- cargo install --force --all-features cargo-deny --locked
buildah run $CONTAINER_ID -- cargo install --force --all-features sqlx-cli --locked
buildah run $CONTAINER_ID -- cargo install --force --all-features wasm-pack

# squash it
buildah commit --squash $CONTAINER_ID $FQ_IMAGE_NAME
buildah commit --squash $CONTAINER_ID $COMMIT_IMAGE_NAME

# push to gitlab's container registry
buildah push $FQ_IMAGE_NAME
buildah push $COMMIT_IMAGE_NAME

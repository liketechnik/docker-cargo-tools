<!--
SPDX-FileCopyrightText: 2019 Major Hayden
SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: Apache-2.0
-->

# docker-cargo-tools container
 
 Automatically build and publish a docker container containing common cargo extensions; used for building rust packages in docker containers.

## Features

- contains:
    - cargo-make
    - cargo-tarpaulin
    - cargo-chef
    - cargo-audit
    - cargo-deny
    - sqlx-cli
    - wasm-pack
- based on either `rust:latest` or `rust:bullseye`

## Usage

Feel free to use these images in your CI pipelines.
The images should (usually) be kept up-to-date, by weekly executing this repo's ci pipeline, which rebuilds the images.
If you miss any tool in the selection above, feel free to open an issue, or just open a PR adding the install of the respective package :)

## Acknowledgements

Building with `buildah` based on https://gitlab.com/majorhayden/os-containers.
